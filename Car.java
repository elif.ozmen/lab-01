
public class Car {
	private double odometer;
    private String brand;
    private String color;
    private int gear;
    private double totalNumberOfHoursDriven;

    public Car() {
        odometer = 0.;
        brand = "Renault";
        color = "White";
        gear = 0;
        totalNumberOfHoursDriven = 0.;
    }

    public Car(String b) {
        odometer = 0.;
        brand = b;
        color = "White";
        gear = 0;
        totalNumberOfHoursDriven = 0.;
    }

    public Car(String b, String c) {
        odometer = 0.;
        brand = b;
        color = c;
        gear = 0;
        totalNumberOfHoursDriven = 0.;
    }

    public void incrementGear() {
        if (gear < 5) {
            gear += 1;
        } else {
            System.out.println("Error: the gear value must be in range [0, 5].");
        }
    }

    public void decrementGear() {
        if (gear > 0) {
            gear -= 1;
        } else {
            System.out.println("Error: the gear value must be in range [0, 5].");
        }
    }

    public void drive(double numberOfHoursTravelled, double kmTravelledPerHour) {
        totalNumberOfHoursDriven += numberOfHoursTravelled;
        odometer += numberOfHoursTravelled * kmTravelledPerHour;
    }

    public double getOdometer() {
        return odometer;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String c) {
        color = c;
    }

    public int getGear() {
        return gear;
    }

    public double getTotalNumberOfHoursDriven() {
        return totalNumberOfHoursDriven;
    }

    public void display() {
        System.out.println("The " + brand + " has color " + color + "."
                + " It has travelled " + odometer + " kms so far."
                + " It is at gear " + gear + "."
                + " The car has been driven for " + totalNumberOfHoursDriven + " hours.");
    }

    public double getAverageSpeed() {
        return odometer / totalNumberOfHoursDriven;
    }


}
